import { Component, OnInit, Inject, Renderer2, AfterViewChecked } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser'
import { todo } from '../todo';
import { tasks } from '../todo-array';
declare var $: any
import * as moment from "moment"
moment.locale('ru')
@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  constructor(private _renderer2: Renderer2, @Inject(DOCUMENT) private _document) { }
  // task: todo = {
  //   task_name: 'Test task',
  //   task_description: "Test task description",
  //   task_time: "10.10.18"
  // };

  tasks = tasks;
  selectedTask: todo;
  onSelect(task: todo): void {
    this.selectedTask = task;
  }
  ngAfterViewChecked() {

  }
  ngOnInit() {
    setInterval(() => {
      this.checkItemDate();
    }, 5000)
  }
  checkItemDate() {
    this.tasks.forEach(item => {
      let taskDate = item.task_time.split('.');
      let nowDate = moment().format('DD.MM.YYYY').split('.');
      if(taskDate[1] === nowDate[1] && taskDate[2]===nowDate[2]) {
        let sub = parseInt(taskDate[0]) - parseInt(nowDate[0]);
        if(sub >=0 && sub <=3) {
          item.yellow=true;
        }   else if(sub <=-1) {
              item.red=true;
        }
      }
    })
  }

  onSubmit(form: any): void {
    if (!form.task_name || !form.task_description || !form.task_time) {
      console.log('Empty data')
    } else {
      let obj = {
        task_name: form.task_name,
        task_description: form.task_description,
        task_time: moment(form.task_time,'"DD.MM.YY').format('DD.MM.YYYY')
      }
      this.tasks.push(obj);
    }
  }
  onImageClick(item:any):void {
    let removeInd = this.tasks.map(el => {return el.task_name}).indexOf(item.task_name);
    this.tasks.splice(removeInd,1);
  }
  OnSelectClose():void {
    this.selectedTask = null;
  }
}
