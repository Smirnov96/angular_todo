import { todo } from './todo';
import * as moment from 'moment'
moment.locale('ru')
export const tasks: todo[] = [
    {
        task_name: 'Test task1',
        task_description: "Test task description",
        task_time: moment("12.02.2018","DD.MM.YYYY").format('L')
    },
    {
        task_name: 'Test task2',
        task_description: "Test task description",
        task_time: moment("12.03.2018","DD.MM.YYYY").format('L'),
        
    },
    {
        task_name: 'Test task3',
        task_description: "Test task description",
        task_time: moment("15.02.2018","DD.MM.YYYY").format('L'),
        
    },
    {
        task_name: 'Test task4',
        task_description: "Test task description",
        task_time: moment("17.02.2018","DD.MM.YYYY").format('L'),
    },
    {
        task_name: 'Test task5',
        task_description: "Test task description",
        task_time: moment("14.07.2018","DD.MM.YYYY").format('L'),
        
    },
    {
        task_name: 'Test task6',
        task_description: "Test task description",
        task_time: moment("11.02.2018","DD.MM.YYYY").format('L'),
        
    },
    {
        task_name: 'Test task7',
        task_description: "Test task description",
        task_time: moment("12.02.2018","DD.MM.YYYY").format('L'),
        
    },
]